package assignment4;
import java.util.Scanner;

interface FindPower
{
	public double power(int x, int y); 
}

public class LambdaDemo {

	public static void main(String args[])
	{
		FindPower obj = (x,y) -> Math.pow(x, y);
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a: ");
		int a = sc.nextInt();
		System.out.print("Enter b: ");
		int b = sc.nextInt();
		sc.close();
		System.out.println(a+" to the power of "+b+" : "+obj.power(a, b));
	} 
}
