package assignment4;
import java.util.Scanner;
interface Authentication
{
	public String username = "keshabh";
	public String password = "abcd123";
	public boolean authenticate(String user, String pass);
}

public class LambdaDemo3 {
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter username: ");
		String user = sc.next();
		System.out.print("Enter Password: ");
		String pass = sc.next();
		
		Authentication obj = (u,p) -> {
			
			if(u.toLowerCase().equals(Authentication.username) && p.equals(Authentication.password))
				return true;
			else
				return false;
		};
		if(obj.authenticate(user, pass))
			System.out.println("User Authenticated");
		else
			System.out.println("Invalid Username or Password");
		
	}
}
