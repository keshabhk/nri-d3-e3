package assignment4;
import java.util.Scanner;
interface Formatter
{
	public String format(String word);
}

public class LambdaDemo2 {
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a word: ");
		String word  = sc.next();
		
		Formatter obj = (x) -> x.replace(""," ").trim();
		System.out.println("Formatted word :" +obj.format(word));

		
	}

}
