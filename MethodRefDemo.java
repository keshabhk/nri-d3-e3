package assignment4;

class Employee
{
	private int empid;
	private String name;
	private int sal;
	public Employee(int empid, String name, int sal) {
		super();
		this.empid = empid;
		this.name = name;
		this.sal = sal;
	}
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSal() {
		return sal;
	}
	public void setSal(int sal) {
		this.sal = sal;
	}
	
	
}



interface A {
	public boolean show();
}

class Digit {
	public static boolean isSingleDigit() {
	    return true;
	}
}

public class MethodRefDemo {

	public static void main(String[] args) {
		
		A a2 = Digit::isSingleDigit;
		System.out.println(a2.show());
	}
}
